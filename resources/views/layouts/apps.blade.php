<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ env('APP_NAME') }} - @yield('title','Dashboard') </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App css -->
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" id="light-style" />
    <link href="{{ asset('assets/css/app-dark.min.css') }}" rel="stylesheet" type="text/css" id="dark-style" />
    
    <!-- Datatable -->
    <link href="{{ asset('assets/css/vendor/dataTables.bootstrap5.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/vendor/responsive.bootstrap5.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/vendor/buttons.bootstrap5.css') }}" rel="stylesheet" type="text/css" />

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Toaster CDN -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    @stack('pageCss')


</head>

<body class="loading"
    data-layout-config='{"leftSideBarTheme":"dark","layoutBoxed":false, "leftSidebarCondensed":false, "leftSidebarScrollable":false,"darkMode":false, "showRightSidebarOnStart": true}'>
    <!-- Begin page -->
    <div class="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
        @include('partials.left_sidebar')
        <!-- Left Sidebar End -->
        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <div class="content">
                <!-- Topbar Start -->
                @include('partials.topbar')
                <!-- end Topbar -->

                <!-- Start Content-->
                <div class="container-fluid">

                    <!-- start main content-->
                    @yield('content')
                    <!-- end main content -->

                </div> <!-- container -->

            </div> <!-- content -->

            <!-- Footer Start -->
            @include('partials.footer')
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->


    <!-- Right Sidebar -->
    @include('partials.right_sidebar')
    <!-- /End-bar -->


    <!-- bundle -->
    <script src="{{asset('assets/js/vendor.min.js') }}"></script>
    <script src="{{asset('assets/js/app.min.js') }}"></script>
    
 
<!-- Datatables js -->
<script src="{{asset('assets/js/vendor/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('assets/js/vendor/dataTables.bootstrap5.js') }}"></script>
<script src="{{asset('assets/js/vendor/dataTables.responsive.min.js') }}"></script>
<script src="{{asset('assets/js/vendor/responsive.bootstrap5.min.j') }}s"></script>

<script src="{{asset('assets/js/vendor/dataTables.buttons.min.js') }}"></script>
<script src="{{asset('assets/js/vendor/buttons.bootstrap5.min.js') }}"></script>
<script src="{{asset('assets/js/vendor/buttons.html5.min.js') }}"></script>
<script src="{{asset('assets/js/vendor/buttons.flash.min.js') }}"></script>
<script src="{{asset('assets/js/vendor/buttons.print.min.js') }}"></script>

<!-- Datatable Init js -->
<script src="{{asset('assets/js/pages/demo.datatable-init.js') }}"></script>
<!-- Dropdown -->

 <!-- Validation CDN-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.js"></script>
   
@yield('scripts')
    @stack('pageJs')
</body>

</html>