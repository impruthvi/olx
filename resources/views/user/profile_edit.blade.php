
@extends('layouts.apps')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Hyper</a></li>
                    <li class="breadcrumb-item active">Edit Profile</li>
                </ol>
            </div>
            <h4 class="page-title">Edit Profile</h4>
        </div>
    </div>
</div>

<div class="row">

    <div class="flex-wrap pt-1">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                   <!-- end nav-->
                    <div class="tab-content">
                        <div class="tab-pane show active" id="basic-form-preview">
                            <form id="profile_edit" action="{{ route('profile.update') }}" method="post"  enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <div class="container">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="name" class="form-label">Name</label>
                                                    <input type="text" class="form-control" id="name" aria-describedby="amountHelp" placeholder="Enter Product Name" name="name" value="{{ Auth::user()->name }}"> 
                                                    @if($errors->has('name'))
                                                    <div class="text-danger">
                                                        Please Ente Name.
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="email" class="form-label">Email</label>
                                                    <input type="email" class="form-control" id="email" aria-describedby="priceHelp" placeholder="Enter Amount" name="email"  value="{{ Auth::user()->email }}"> 
                                                    @if($errors->has('email'))
                                                    <div class="text-danger">
                                                        Please Enter Email Address.
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="number" class="form-label">Number</label>
                                                    <input type="text" class="form-control" id="number" aria-describedby="amountHelp" placeholder="Enter Product Name" name="number" value="{{ Auth::user()->number }}"> 
                                                    @if($errors->has('number'))
                                                    <div class="text-danger">
                                                        Please Ente Number.
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="city" class="form-label">City</label>
                                                    <input type="city" class="form-control" id="city" aria-describedby="priceHelp" placeholder="Enter Amount" name="city"  value="{{ Auth::user()->city }}"> 
                                                    @if($errors->has('city'))
                                                    <div class="text-danger">
                                                        Please Enter City.
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="state" class="form-label">State</label>
                                                    <input type="text" class="form-control" id="state" aria-describedby="amountHelp" placeholder="Enter Product Name" name="state" value="{{ Auth::user()->state }}"> 
                                                    @if($errors->has('state'))
                                                    <div class="text-danger">
                                                        Please Ente State.
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="country" class="form-label">Country</label>
                                                    <input type="country" class="form-control" id="country" aria-describedby="priceHelp" placeholder="Enter Amount" name="country"  value="{{ Auth::user()->country }}"> 
                                                    @if($errors->has('country'))
                                                    <div class="text-danger">
                                                        Please Enter Country.
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="profile" class="form-label">Profile</label>
                                                    <input type="file" class="form-control" id="profile" name="profile" value="{{ old('image') }}"> 
                                                    @if($errors->has('profile'))
                                                    <div class="text-danger">
                                                        Please Enter profile.
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="address" class="form-label">Address</label>
                                                    <textarea class="form-control" placeholder="Enter a address" id="floatingTextarea" style="height: 100px;" name="address" >{{ Auth::user()->address }}</textarea>
    
                                                    @if($errors->has('address'))
                                                    <div class="text-danger">
                                                        Please Enter Address.
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>



                                        </div>

                                        



                                        <div class="row">
                                            <div class="col-sm">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                            </form>



                            </div>
                            <!-- end preview-->

                            <!-- end preview code-->
                        </div>
                        <!-- end tab-content-->

                    </div>
                    <!-- end card-body-->
                </div>
                <!-- end card-->
            </div>
        </div>

    </div>
@endsection 
    @section('scripts')

        <!-- Success message -->


@if (session('profile.update'))

<script>
    toastr.options = {
        "closeButton": true,
        "progressBar": true
    }
    toastr.success("Updated successfully");
</script>
{{ session()->forget('profile.update') }} 
@endif






    @endsection