@extends('layouts.apps')
 @section('content')
<div class="row">

    <div class="flex-wrap pt-1">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                   <!-- end nav-->
                    <div class="tab-content">
                        <div class="tab-pane show active" id="basic-form-preview">
                            <form id="product" action="{{ route('resetpassword.store_reset') }}" method="post"  enctype="multipart/form-data">
                                @method('PATCH')
                                @csrf
                                <div class="container">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="old_password" class="form-label">Old Password</label>
                                                    <input type="password" name="old_password" id="old_password" class="form-control" id="old_password" aria-describedby="amountHelp" placeholder="Enter Old Password" value="{{ old('old_password') }}"> 
                                                    @if($errors->has('old_password'))
                                                    <div class="text-danger">
                                                        {{$errors->first('old_password')}}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="password" class="form-label">Password</label>
                                                    <input type="password" class="form-control" id="password" aria-describedby="priceHelp" placeholder="Enter Password" name="password" value="{{ old('password') }}"> 
                                                    @if($errors->has('password'))
                                                    <div class="text-danger">
                                                        {{$errors->first('password')}}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="password_confirmation" class="form-label">Confirm Password</label>
                                                    <input type="password" class="form-control" id="password_confirmation" aria-describedby="priceHelp" placeholder="Enter Confirm Password" name="password_confirmation" value="{{ old('password_confirmation') }}"> 
                                                    @if($errors->has('password_confirmation'))
                                                    <div class="text-danger">
                                                       {{$errors->first('password_confirmation')}}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row">
                                            <div class="col-sm">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                            </form>



                            </div>
                            <!-- end preview-->

                            <!-- end preview code-->
                        </div>
                        <!-- end tab-content-->

                    </div>
                    <!-- end card-body-->
                </div>
                <!-- end card-->
            </div>
        </div>

    </div>


    @endsection 
    @section('scripts')


    {{-- <script>
        $(function() {

            $("#product").validate({
                rules: {
                    name: {
                        required: true,
                        lettersonly: true
                    },
                    price: {
                        required: true,
                        number: true
                    },
                    cat_id: {
                        required: true,
                        digits: true
                    },
                    description: {
                        required: true,
                        minlength: 3,
                        maxlength: 500,
                        
                    },
                    image: {
                        required: true,
                        accept:"jpg,png,jpeg"
                    }

                }
            })
        });
    </script> --}}


        @if (session('password.update')=='fail')

        <script>
            toastr.options = {
                "closeButton": true,
                "progressBar": true
            }
            toastr.error("Password not match");
        </script>
        {{ session()->forget('password.update') }} 
        @endif

    @endsection