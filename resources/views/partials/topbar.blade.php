<div class="navbar-custom">
    <ul class="list-unstyled topbar-menu float-end mb-0">
        <li class="notification-list">
            <a class="nav-link end-bar-toggle" href="javascript: void(0);">
                <i class="dripicons-gear noti-icon"></i>
            </a>
        </li>
           

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user arrow-none me-0" data-bs-toggle="dropdown" href="#"
                role="button" aria-haspopup="false" aria-expanded="false">
                <span class="account-user-avatar">
                    <img src="{{Auth::user()->profile ? Storage::url(Auth::user()->profile) : asset('assets/images/users/avatar-1.jpg') }}" alt="user-image" class="rounded-circle">
                </span>
                <span>
                    <span class="account-user-name">{{ Auth::user()->name }}</span>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-end dropdown-menu-animated topbar-dropdown-menu profile-dropdown">
                <!-- item-->
                <div class=" dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Welcome !</h6>
                </div>

                <a href="{{ route('profile') }}" class="dropdown-item">
                    <i class="mdi mdi-account-circle me-1"></i>
                    <span>My Account</span>
                </a>

                <a href="{{ route('password.reset') }}" class="dropdown-item">
                    <i class="uil-key-skeleton-alt"></i>
                    <span>Change Password</span>
                </a>
                @if(session('user.change')=='seller')
                <form action="{{ route('user.change') }}" method="post">
                    @csrf
                    <button type="submit" class="dropdown-item" name="submit" value='buyer'><i class="uil-key-skeleton-alt"></i><span>Buyer</span></button>
                </form>
                @else
   
                    <form action="{{ route('user.change') }}" method="post">
                        @csrf
                        <button type="submit" class="dropdown-item" name="submit" value='seller'><i class="uil-key-skeleton-alt"></i><span>Seller</span></button>
                    </form>
                @endif

           
            

                <!-- item-->
          <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                        <i class="mdi mdi-logout me-1"></i>
                 
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
            </div>
        </li>

    </ul>
    <button class="button-menu-mobile open-left">
        <i class="mdi mdi-menu"></i>
    </button>

  

    <div class="app-search dropdown d-none d-lg-block">
        <form method="get" action="{{ route('product.search')}}">
            <div class="input-group">
                <input type="text" class="form-control dropdown-toggle" placeholder="Search..." id="top-search" name="search">
                <span class="mdi mdi-magnify search-icon"></span>
                <button class="input-group-text btn-primary" type="submit">Search</button>
            </div>
        </form>

    </div>

   
</div>