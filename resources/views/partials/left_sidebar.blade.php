<div class="leftside-menu">

    @if(session('user.change')=='seller')
    <!-- LOGO -->
    <a href="{{ route('products') }}" class="logo text-center logo-light">
        <span class="logo-lg">
            <img src="{{ asset('assets/images/logo.png') }}" alt="" height="16">
        </span>
        <span class="logo-sm">
            <img src="{{ asset('assets/images/logo_sm.png') }}" alt="" height="16">
        </span>
    </a>

    <a href="{{ route('products') }}" class="logo text-center logo-dark">
        <span class="logo-lg">
            <img src="{{ asset('assets/images/logo-dark.png') }}" alt="" height="16">
        </span>
        <span class="logo-sm">
            <img src="{{ asset('assets/images/logo_sm_dark.png') }}" alt="" height="16">
        </span>
    </a>

    @else
    <a href="#" class="logo text-center logo-light">
        <span class="logo-lg">
            <img src="{{ asset('assets/images/logo.png') }}" alt="" height="16">
        </span>
        <span class="logo-sm">
            <img src="{{ asset('assets/images/logo_sm.png') }}" alt="" height="16">
        </span>
    </a>

    <a href="#" class="logo text-center logo-dark">
        <span class="logo-lg">
            <img src="{{ asset('assets/images/logo-dark.png') }}" alt="" height="16">
        </span>
        <span class="logo-sm">
            <img src="{{ asset('assets/images/logo_sm_dark.png') }}" alt="" height="16">
        </span>
    </a>



    @endif

    <!-- LOGO -->

    
    <div class="h-100" id="leftside-menu-container" data-simplebar>

        <!--- Sidemenu -->
        <ul class="side-nav">

            <li class="side-nav-title side-nav-item">Navigation</li>

            @if(session('user.change')=='seller')
            <li class="side-nav-item">
                <a data-bs-toggle="collapse" href="#sidebarDashboards2" aria-expanded="false"
                    aria-controls="sidebarDashboards2" class="side-nav-link">
                    <i class="uil-money-bill-stack"></i>
                    <span class="menu-arrow"></span>
                    <span> Product </span>
                </a>
                <div class="collapse" id="sidebarDashboards2">
                    <ul class="side-nav-second-level">
                        <li>
                            <a href="{{ route('product.create') }}">Create</a>
                        </li>
                        <li>
                            <a href="{{ route('product.index') }}">All Product</a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif

            <li class="side-nav-item">
                <a data-bs-toggle="collapse" href="#sidebarDashboards" aria-expanded="false"
                    aria-controls="sidebarDashboards" class="side-nav-link">
                    <i class="uil-money-bill-stack"></i>
                    <span class="menu-arrow"></span>
                    <span> Purchase </span>
                </a>
                <div class="collapse" id="sidebarDashboards">
                    <ul class="side-nav-second-level">
                        @if(session('user.change')=='seller')
                        
                            <li>
                                <a href="{{ route('purchase.request') }}">Requests</a>
                            </li>
                            <li>
                                <a href="{{ route('purchase.soldout') }}">Sold</a>
                            </li>
                            <li>
                                <a href="{{ route('purchase.index') }}">All</a>
                            </li>
                        @else    
                            <li>
                                <a href="{{ route('purchase.pending') }}">Pending</a>
                            </li>
                            
                            <li>
                                <a href="{{ route('purchase.purchased') }}">Purchased</a>
                            </li>
                        @endif

                        

                     
                        
                    </ul>
                </div>
            </li>


           


           
        </ul>

        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>