@extends('layouts.apps')
@section('content')
<div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Hyper</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Projects</a></li>
                                            <li class="breadcrumb-item active">Projects List</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Projects</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title --> 

                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <a href="{{ route('product.create') }}" class="btn btn-danger rounded-pill mb-3"><i class="mdi mdi-plus"></i> Create Project</a>
                            </div>
                            {{-- <div class="col-sm-8">
                                <div class="text-sm-end">
                                    <div class="btn-group mb-3">
                                        <button type="button" class="btn btn-primary">All</button>
                                    </div>
                                    <div class="btn-group mb-3 ms-1">
                                        <button type="button" class="btn btn-light">Ongoing</button>
                                        <button type="button" class="btn btn-light">Finished</button>
                                    </div>
                                    <div class="btn-group mb-3 ms-2 d-none d-sm-inline-block">
                                        <button type="button" class="btn btn-secondary"><i class="dripicons-view-apps"></i></button>
                                    </div>
                                    <div class="btn-group mb-3 d-none d-sm-inline-block">
                                        <button type="button" class="btn btn-link text-muted"><i class="dripicons-checklist"></i></button>
                                    </div>
                                </div>
                            </div><!-- end col--> --}}
                        </div> 
                        <!-- end row-->

                        <!-- end row-->
                        
                        <div class="row">
                    
                        @foreach ($products as $product)
              
                            
                               
                            <div class="col-md-6 col-xxl-3">
                                <a href="{{route('product.show',$product->id)}}"><!-- project card -->
                                <div class="card d-block">
                                    <!-- project-thumbnail -->
                                    <img class="card-img-top" src="{{ $product->images ? Storage::url($product->images->path) : 'https://via.placeholder.com/400C/O' }}" alt="project image cap">
                                    <!-- <div class="card-img-overlay">
                                        <div class="badge bg-secondary text-light p-1">Ongoing</div>
                                    </div> -->

                                    <div class="card-body position-relative">
                                        <!-- project title-->
                                        <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title">{{$product->name}}</a>
                                        </h4>

                                        <!-- project detail-->
                                        <p class="mb-3">
                                            <span class="pe-2 text-nowrap">
                                                <!-- <i class="uil-pricetag-alt"></i> -->

                                                ₹ <b>{{$product->price}}</b> 
                                            </span>
                                            
                                        </p>

                                        <p class="mb-3">
                                            <span class="text-nowrap">
                                                <i class="mdi mdi-comment-multiple-outline"></i>
                                                {{$product->description}}
                                            </span>
                                            
                                        </p>
           
                                        <div>                              
                                  
                            
                                
                                        </div>
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                                </a>
                                
                            </div> <!-- end col -->

                            
                            @endforeach
                       
                        
                        

                        </div>
                        <!-- end row-->
                        
                    </div>
@endsection