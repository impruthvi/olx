
@extends('layouts.apps')
 @section('content')

<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-between align-items-center">
            <div class="page-title-box">
                <h4 class="page-title">All Products</h4>
            </div>
            
                <a href="{{ route('product.create') }}">
                    <button class="btn btn-secondary buttons-print" tabindex="0" aria-controls="datatable-buttons" type="button">
                        <span>Add</span>
                    </button>
                </a>
            
        </div>
       
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap w-100">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Seller</th>
                    <th>Category</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Created</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td><img height="50" src="{{ $product->images ? Storage::url($product->images->path) : 'https://via.placeholder.com/400C/O' }}" alt=""></td>
                    <td>{{ $product->user->name }}</td>
                    <td>{{ $product->category->name }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->description }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->created_at->diffForHumans() }}</td>
                    <td class="table-action">
                        <div class="row">
                            <div class="col-3">
                                <button type="submit" class="btn gb-transparent"> <a href="{{ route('product.edit',$product->id) }}"class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a></button>
                            </div>
                            <div class="col-3">
                                <form method="post" action="{{ route('product.destroy',$product->id) }}"> 
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn gb-transparent"> <a href="" class="action-icon"> <i class="mdi mdi-delete"></i></a></button>
                                </form>
                            </div>
                            
                          </div>                       
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>





@endsection



@section('scripts')

<!-- Success message -->
@if (session('product.delete'))

<script>
    toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true
  }
  		toastr.success("Deleted successfully");
</script>
{{ session()->forget('product.delete') }}
@endif



<!-- Success message -->
@if (session('product.update'))

<script>
    toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true
  }
  		toastr.success("Updated successfully");
</script>
{{ session()->forget('product.update') }}
@endif

 <!-- Success message -->
 @if (session('product_added'))

 <script>
     toastr.options = {
         "closeButton": true,
         "progressBar": true
     }
     toastr.success("Added successfully");
 </script>
 {{ session()->forget('product_added') }} 
 @endif


    
@endsection


