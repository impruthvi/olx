@extends('layouts.apps')
@section('content')
<div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Hyper</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">eCommerce</a></li>
                                            <li class="breadcrumb-item active">Product Details</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Product Details</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title --> 

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <!-- Product image -->
                                                <a href="javascript: void(0);" class="text-center d-block mb-4">
                                                    <img src="{{ $product->images ? Storage::url($product->images->path) : 'https://via.placeholder.com/400C/O' }}" class="img-fluid" style="max-width: 280px;" alt="Product-img">
                                                </a>

                                                <!-- <div class="d-lg-flex d-none justify-content-center">
                                                    <a href="javascript: void(0);">
                                                        <img src="assets/images/products/product-1.jpg" class="img-fluid img-thumbnail p-2" style="max-width: 75px;" alt="Product-img">
                                                    </a>
                                                    <a href="javascript: void(0);" class="ms-2">
                                                        <img src="assets/images/products/product-6.jpg" class="img-fluid img-thumbnail p-2" style="max-width: 75px;" alt="Product-img">
                                                    </a>
                                                    <a href="javascript: void(0);" class="ms-2">
                                                        <img src="assets/images/products/product-3.jpg" class="img-fluid img-thumbnail p-2" style="max-width: 75px;" alt="Product-img">
                                                    </a>
                                                </div> -->
                                            </div> <!-- end col -->
                                            <div class="col-lg-7">
                                                
                                                    <!-- Product title -->
                                                    <h3 class="mt-0">{{$product->name}}</h></h3>
                                                    <p class="mb-1">Added Date: {{$product->created_at->diffForHumans()}}</p>
                                                   

                                                    <!-- Product stock -->
                                                    <div class="mt-3">
                                                        <!-- <h4><span class="badge badge-success-lighten">Instock</span></h4> -->
                                                    </div>

                                                    <!-- Product description -->
                                                    <div class="mt-4">
                                                        <h6 class="font-14">Price:</h6>
                                                        <h3> ₹{{$product->price}}</h3>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="row">
                                                            
                                                            <div class="mt-4 col-3">
                                                                <h6 class="font-14">Description:</h6>
                                                                <p>{{$product->description}} </p>
                                                            </div>
                                                            <div class="mt-4 col-3">
                                                                <h6 class="font-14">Category:</h6>
                                                                <p>{{$product->category->name}}</p> 
                                                            </div>

                                                            <div class="mt-4 col-3">
                                                                <h6 class="font-14">Seller:</h6>
                                                                <p>{{$product->user->name}}</p>
                                                            </div>
                                                        </div>     
                                                    </div>
                                                   

                                        

                                                    @if($purchase=='')

                                                        <form action="{{ route('purchase.store')}}" method="post" enctype="multipart/form-data">
                                                            @csrf
                                                            <input type="text" name="product_id" value="{{$product->id}}" hidden>
                                                            <input type="text" name="seller_id" value="{{$product->seller_id}}" hidden>
                                                            <input type="text" name="buyer_id" value="{{Auth::user()->id}}" hidden>
                                                            <button type="submit" class="btn btn-primary">Request</button> 
                                                        </form>
                                                    @elseif ($purchase->status=='pending')
                                                            <div class="mt-3">
                                                                <h4><span class="badge badge-success-lighten">Pending</span></h4>
                                                            </div>

                                                    @elseif ($purchase->status=='accepted')

                                                        <form action="{{ route('purchase.update',$purchase->id) }}" method="post" enctype="multipart/form-data">
                                                        @method('PUT')    
                                                        @csrf
                                                            <input type="text" name="status" value="purchased" hidden>
                                                            
                                                            <button type="submit" class="btn btn-primary">Buy</button> 
                                                        </form>

                                                        @elseif ($purchase->status=='purchased')
                                                            <div class="mt-3">
                                                                <h4><span class="badge badge-success-lighten">Purchased</span></h4>
                                                            </div>

                                                    @endif
                                            </div> <!-- end col -->
                                        </div> <!-- end row-->

               
                                        
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end row-->
                        
                    </div>


                    @endsection



                    @section('scripts')

                     
                    
                    <!-- Success message -->
                    @if (session('purchase.update'))
                    
                    <script>
                        toastr.options =
                      {
                          "closeButton" : true,
                          "progressBar" : true
                      }
                              toastr.success("Updated successfully");
                    </script>
                    {{ session()->forget('purchase.update') }}
                    @endif
                    
                    
                        
                    @endsection
                    