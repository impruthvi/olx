@extends('layouts.apps')
@section('content')
<div class="container-fluid">
                        
                        <!-- start page title -->
                        <h4 class="page-title">Product</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">olx</a></li>
                                            <li class="breadcrumb-item active">Products</li>
                                        </ol>
                                    </div>
                                </div>

                                <div class="dropdown btn-group">
                                    <button class="btn btn-light dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort By 
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-animated">
                                        <a class="dropdown-item" href="{{ URL::current() }}">All</a>
                                        <a class="dropdown-item" href="{{ URL::current()."?sort=a_to_z" }}">A to Z</a>
                                        <a class="dropdown-item" href="{{ URL::current()."?sort=price" }}">Price High to Low</a>
                                        <a class="dropdown-item" href="{{ URL::current()."?sort=recent" }}">Recent</a>
                                        <a class="dropdown-item" href="{{ URL::current()."?sort=category" }}">Category</a>
                                    </div>
                                </div>
                                
                            </div>


                            
                        </div>
                        <!-- end page title --> 


                        <!-- end row-->

                        <!-- end row-->
                        
                        <div class="row">
                    
                        @foreach ($products as $product)
              
                            
                               
                            <div class="col-md-6 col-xxl-3">
                                <a href="{{route('product.show',$product->id)}}"><!-- project card -->
                                <div class="card d-block">
                                    <!-- project-thumbnail -->
                                    <img class="card-img-top" src="{{ $product->images ? Storage::url($product->images->path) : 'https://via.placeholder.com/400C/O' }}" alt="project image cap">
                                    <!-- <div class="card-img-overlay">
                                        <div class="badge bg-secondary text-light p-1">Ongoing</div>
                                    </div> -->

                                    <div class="card-body position-relative">
                                        <!-- project title-->
                                        <h4 class="mt-0">
                                            <a href="apps-projects-details.html" class="text-title">{{$product->name}}</a>
                                        </h4>

                                        <!-- project detail-->
                                        <p class="mb-3">
                                            <span class="pe-2 text-nowrap">
                                                <!-- <i class="uil-pricetag-alt"></i> -->

                                                ₹ <b>{{$product->price}}</b> 
                                            </span>
                                            
                                        </p>

                                        <p class="mb-1">
                                            <span class="text-nowrap">
                                                <i class="mdi mdi-comment-multiple-outline"></i>
                                                {{$product->description}}
                                            </span>

                                            
                                            
                                        </p>

                                        <p class="mb-1">
                                            
                                            <span class="text-nowrap">
                                                <i class="uil-tag"></i>
                                                {{$product->category->name}}
                                            </span>
                                            
                                            
                                        </p>
                                        <p class="mb-2">

                                            <span class="text-nowrap">
                                                <i class="  dripicons-user"></i>
                                                {{$product->user->name}}
                                            </span>

                                            <span class="text-nowrap">
                                                <i class="dripicons-clock"></i>
                                                {{$product->created_at->diffForHumans() }}
                                            </span>

                                     
                                            
                                        </p>
           
                                        <div>                              
                                  
                            
                                
                                        </div>
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                                </a>
                                
                            </div> <!-- end col -->

                            
                            @endforeach
                       
                        
                        

                        </div>
                        <!-- end row-->
                        
                    </div>
@endsection