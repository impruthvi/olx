@extends('layouts.apps')
 @section('content')
<div class="row">

    <div class="flex-wrap pt-1">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                   <!-- end nav-->
                    <div class="tab-content">
                        <div class="tab-pane show active" id="basic-form-preview">
                            <form id="product" action="{{ route('product.store') }}" method="post"  enctype="multipart/form-data">
                                @csrf
                                <div class="container">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="name" class="form-label">Name</label>
                                                    <input type="text" class="form-control" id="name" aria-describedby="amountHelp" placeholder="Enter Product Name" name="name" value="{{ old('name') }}"> 
                                                    @if($errors->has('name'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('name')}} 
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="price" class="form-label">Price</label>
                                                    <input type="text" class="form-control" id="price" aria-describedby="priceHelp" placeholder="Enter Amount" name="price" value="{{ old('price') }}"> 
                                                    @if($errors->has('price'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('price')}} 
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            
                                            <div class="col-sm">
                                      
                                                <div class="mb-3">
                                                    <label for="payer" class="form-label">Category</label>
                                                    <select class="form-control select2 select2-hidden-accessible" data-toggle="select2" data-select2-id="select2-data-1-flpv" tabindex="-1" aria-hidden="true" name="cat_id" >
                                                        
                                                        <option value="">Select</option>
                                                                @foreach($categories as $category)
                                                                  @if (old('cat_id') == $category->id)
                                                                      <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                                                  @else
                                                                      <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                                  @endif
                                                              @endforeach
                                                             
                                                                  
                                                         
                                                          </select>
                                                          @if($errors->has('cat_id'))
                                                  
                                                          <div class="text-danger">
                                                             {{$errors->first('cat_id')}}
                                                          </div>
                                                          @endif
                                                </div>
  
                                            </div>

                                            

                                            <div class="col-sm">
                                                <div class="mb-3">
                                                    <label for="image" class="form-label">Image</label>
                                                    <input type="file" class="form-control" id="image" name="image" value="{{ old('image') }}"> 
                                                    @if($errors->has('image'))
                                                    <div class="text-danger">
                                                       {{ $errors->first('image')}}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>



                                        </div>

                                        <div class="col-sm">
                                            <div class="mb-3">
                                                <label for="description" class="form-label">Description</label>
                                                <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea" style="height: 100px;" name="description" >{{ old('description') }}</textarea>

                                                @if($errors->has('description'))
                                                <div class="text-danger">
                                                   {{$errors->first('description')}}
                                                </div>
                                                @endif
                                            </div>
                                        </div>



                                        <div class="row">
                                            <div class="col-sm">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                            </form>



                            </div>
                            <!-- end preview-->

                            <!-- end preview code-->
                        </div>
                        <!-- end tab-content-->

                    </div>
                    <!-- end card-body-->
                </div>
                <!-- end card-->
            </div>
        </div>

    </div>


    @endsection 
    @section('scripts')


    <script>
        $(function() {

            $("#product").validate({
                rules: {
                    name: {
                        required: true,
                        lettersonly: true
                    },
                    price: {
                        required: true,
                        number: true
                    },
                    cat_id: {
                        required: true,
                        digits: true
                    },
                    description: {
                        required: true,
                        minlength: 3,
                        maxlength: 500,
                        
                    },
                    image: {
                        required: true,
                        accept:"jpg,png,jpeg"
                    }

                },
                messages: {
                    name: {
                        required: 'Name is required',
                        lettersonly:'Name must lettersonly'
                    },
                    price: {
                        required: 'Price is required',
                        number:'Price is in only number'
                    },
                    cat_id: {
                        required:'Category is required',
                        digits:'Category content only digits'
                    },
                    description: {
                        required:'Description is required',
                        minlength:'Description minimum length is 3',
                        maxlength:'Description maximum length is 500' 
                    },
                    image: {
                        required:'Image is required',
                        accept:'Image only in jpg,png and jpeg forget'
                    }
                }
            })
        });
    </script>








        <!-- Success message -->
        @if (session('product_added'))

        <script>
            toastr.options = {
                "closeButton": true,
                "progressBar": true
            }
            toastr.success("Added successfully");
        </script>
        {{ session()->forget('product_added') }} 
        @endif
    @endsection