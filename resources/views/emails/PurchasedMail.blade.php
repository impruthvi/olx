@component('mail::message')
# Details

Product Name: {{ $purchase->product->name  }}<br/>
Product Price: {{ $purchase->product->price  }}<br/>
Product Description: {{ $purchase->product->description }}<br/>
Seller Name: {{ $purchase->seller->name}}<br/>


@component('mail::button', ['url' => ''])
Button Text
@endcomponent
{{-- {{ $purchase }} --}}
Thanks,<br>
{{ config('app.name') }}
@endcomponent
