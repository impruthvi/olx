@extends('layouts.apps')
 @section('content')

<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-between align-items-center">
            <div class="page-title-box">
                <h4 class="page-title">Sold</h4>
            </div>            
        </div>

        <div class="dropdown btn-group">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-between align-items-center">

                        <div class="col-sm">
                            <div class="mb-3">
                                <label for="payer" class="form-label">Type</label>
                                <select class="form-control" aria-hidden="true" name="type" id="select_type" >
                                        @if (old('type') == 'year')
                                            <option value="year" selected>Year</option>
                                            <option value="month">Month</option>
                                        @elseif(old('type') == 'month')
                                            <option value="year">Year</option>
                                            <option value="month" selected>Month</option>
                                        @else
                                            <option value="">Select</option>
                                            <option value="week">week</option>
                                            <option value="year">Year</option>
                                            <option value="month">Month</option>
                                        @endif
                                  </select>
            
                        
            
                                @if($errors->has('type'))
                            
                                    <div class="text-danger">
                                        Please choose a Type.
                                    </div>
                                @endif
                              </div>
                          </div>
                      </div>
                       
                      <div class="col-sm type" id="year">
                        <div class="mb-3">
                              <label for="payer" class="form-label">Year</label>
                              <form action="{{ URL::current() }}" method="get">

                                <select id="ddlYears" class="form-control" name="year"></select>
                                <input class="form-control date" id="example-year" type="submit">
                            </form>
                          </div>
                      </div>
            
                      <div class="col-sm type" id="month">
                        <div class="mb-3">
                            <label for="month" class="form-label">Month</label>
                            <form action="{{ URL::current() }}" method="get">

                                <input class="form-control" id="example-month" type="month" name="month">
                                <input class="form-control" id="example-month" type="submit">
                            </form>
                            
                        </div>
                      </div>

                      <div class="col-sm type" id="week">
                        <div class="mb-3">
                            <label for="week" class="form-label">Week</label>
                            <form action="{{ URL::current() }}" method="get">

                                <input type="text" class="form-control date" id="singledaterange" data-toggle="date-picker" data-cancel-class="btn-warning" name="week">
                                <input class="form-control" id="example-week" type="submit">
                            </form>
                            
                        </div>
                      </div>
                        
                    </div>
                </div>
            </div>
           
            



        </div>
       
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap w-100">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Product Name</th>
                    <th>Buyer Name</th>
                    <th>Buyer Number</th>
                    <th>Status</th>
                    <th>Created</th>
                </tr>
            </thead>

            <tbody>
                @foreach($purchases as $purchase)
                <tr>
                    <td>{{ $purchase->id }}</td>
                    <td>{{ $purchase->product->name }}</td>
                    <td>{{ $purchase->buyer->name }}</td>
                    <td>{{ $purchase->buyer->number }}</td>
                    <td>{{ $purchase->status }}</td>
                    <td>{{ $purchase->created_at->diffForHumans() }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>





@endsection

@section('scripts')


<script>
    $(document).ready(function () {
        $('.type').hide();
        
        if($('#select_type').val()=='year'){
            
            $('#year').show();
        }else if($('#select_type').val()=='month'){
            $('#month').show();
        }else if($('#select_type').val()=='week'){
            $('#week').show();
        }
        
        $('#select_type').change(function () {
            $('.type').hide();
            $('#'+$(this).val()).show();
        })
    });
</script>

<script type="text/javascript">
    $(function () {
        //Reference the DropDownList.
        var ddlYears = $("#ddlYears");
 
        //Determine the Current Year.
        var currentYear = (new Date()).getFullYear();
 
        //Loop and add the Year values to DropDownList.
        for (var i = 1950; i <= currentYear; i++) {
            var option = $("<option />");
            option.html(i);
            option.val(i);
            ddlYears.append(option);
        }
    });
</script>

@endsection


