@extends('layouts.apps')
 @section('content')

<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-between align-items-center">
            <div class="page-title-box">
                <h4 class="page-title">Requests</h4>
            </div>            
        </div>
       
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap w-100">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Product Name</th>
                    <th>Buyer Name</th>
                    <th>Buyer Number</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($purchases as $purchase)
                <tr>
                    <td>{{ $purchase->id }}</td>
                    <td>{{ $purchase->product->name }}</td>
                    <td>{{ $purchase->buyer->name }}</td>
                    <td>{{ $purchase->buyer->number }}</td>
                    <td>{{ $purchase->status }}</td>
                    <td>{{ $purchase->created_at->diffForHumans() }}</td>
                    <td class="table-action">
                        <div class="row">
                            <div class="col-3">
                                <button type="submit" class="btn gb-transparent"> <a href="{{ route('purchase.edit',$purchase->id) }}"class="action-icon"><i class="mdi mdi-square-edit-outline"></i></a></button>
                            </div>
                            <div class="col-3">
                                <form method="post" action="{{ route('purchase.destroy',$purchase->id) }}"> 
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn gb-transparent"> <a href="" class="action-icon"> <i class="mdi mdi-delete"></i></a></button>
                                </form>
                            </div>
                            
                          </div>                       
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>





@endsection



@section('scripts')

<!-- Success message -->
@if (session('purchase.delete'))

<script>
    toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true
  }
  		toastr.success("Deleted successfully");
</script>
{{ session()->forget('purchase.delete') }}
@endif



<!-- Success message -->
@if (session('purchase.update'))

<script>
    toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true
  }
  		toastr.success("Updated successfully");
</script>
{{ session()->forget('purchase.update') }}
@endif


    
@endsection


