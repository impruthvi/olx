@extends('layouts.apps')
 @section('content')

<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-between align-items-center">
            <div class="page-title-box">
                <h4 class="page-title">Pendinng</h4>
            </div>            
        </div>
       
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap w-100">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Seller</th>
                    <th>Status</th>
                    <th>Created</th>
                </tr>
            </thead>

            <tbody>
                @foreach($purchases as $purchase)
                <tr>
                    <td>{{ $purchase->id }}</td>
                    <td>{{ $purchase->product->name }}</td>
                    <td>{{ $purchase->seller->name }}</td>
                    <td>{{ $purchase->status }}</td>
                    <td>{{ $purchase->created_at->diffForHumans() }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>





@endsection


