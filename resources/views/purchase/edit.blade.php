@extends('layouts.apps')
 @section('content')
<div class="row">

    <div class="flex-wrap pt-1">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                   <!-- end nav-->
                    <div class="tab-content">
                        <div class="tab-pane show active" id="basic-form-preview">
                            <form id="purchase" action="{{ route('purchase.update',$purchase->id) }}" method="post"  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="container">
                                    <div class="row">

                                        <div class="row">

                                            
                                            <div class="col-sm">
                                      
                                                <div class="mb-3">
                                                    <label for="payer" class="form-label">Status</label>
                                                    <select class="form-control select2 select2-hidden-accessible" data-toggle="select2" data-select2-id="select2-data-1-flpv" tabindex="-1" aria-hidden="true" name="status" >
                                                        
                                                        @if($purchase->status == "pending")
                                                        <option value="">Select</option>
                                                        <option value="pending" selected>Pending</option>
                                                        <option value="accepted">Accept</option>
                                                        <option value="rejected">Rejected</option>

                                                        
                                                        @elseif($purchase->status == "accepted")
                                                        <option value="">Select</option>
                                                        <option value="accepted" selected>Accept</option>
                                                        <option value="pending">Pending</option>
                                                        <option value="rejected">Rejected</option>

                                                        @elseif($purchase->status == "rejected")
                                                        <option value="">Select</option>
                                                        <option value="rejected" selected>Rejected</option>
                                                        <option value="accepted">Accept</option>
                                                        <option value="pending">Pending</option>

                                                        
                                                        @elseif($purchase->status == "purchased")
                                                        <option value="">Select</option>
                                                        <option value="rejected" selected>Rejected</option>
                                                        <option value="pending">Pending</option>
                                                        <option value="accepted">Accept</option>
                                                        @endif                                                 
                                                                  
                                                         
                                                          </select>
                                                          @if($errors->has('cat_id'))
                                                  
                                                          <div class="text-danger">
                                                              Please choose a Category.
                                                          </div>
                                                          @endif
                                                </div>
  
                                            </div>

                                            

                                        </div>




                                        <div class="row">
                                            <div class="col-sm">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>

                                        
                                    </div>
                            </form>



                            </div>
                            <!-- end preview-->

                            <!-- end preview code-->
                        </div>
                        <!-- end tab-content-->

                    </div>
                    <!-- end card-body-->
                </div>
                <!-- end card-->
            </div>
        </div>

    </div>


    @endsection 
    
    @section('scripts')

                     
                    
    <!-- Success message -->
    @if (session('purchase.update'))
    
    <script>
        toastr.options =
      {
          "closeButton" : true,
          "progressBar" : true
      }
              toastr.success("Updated successfully");
    </script>
    {{ session()->forget('purchase.update') }}
    @endif
    
    
        
    @endsection
    