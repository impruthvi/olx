<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::middleware('auth')->group(function () {
    Route::resource('product', ProductController::class);
    Route::resource('purchase', PurchaseController::class);

    Route::get('/products', [ProductController::class, 'display'])->name('products');
    Route::get('/search', [ProductController::class, 'search'])->name('product.search');
    
    Route::get('/soldout', [PurchaseController::class, 'soldOut'])->name('purchase.soldout');
    Route::get('/purchased', [PurchaseController::class, 'purchased'])->name('purchase.purchased');
    Route::get('/pending', [PurchaseController::class, 'pending'])->name('purchase.pending');
    Route::get('/request', [PurchaseController::class, 'request'])->name('purchase.request');
    
    Route::get('/profile', [UserController::class, 'profile'])->name('profile');
    Route::get('/profile/edit', [UserController::class, 'editProfile'])->name('profile.edit');
    Route::get('/passwordreset', [UserController::class, 'passwordReset'])->name('password.reset');
    Route::patch('/passwordreset', [UserController::class, 'storeResetPassword'])->name('resetpassword.store_reset');
    Route::patch('/profile/update', [UserController::class, 'update'])->name('profile.update');
    Route::post('/change', [UserController::class, 'change'])->name('user.change');
});


Route::get('auth/google', [UserController::class, 'authGoogle'])->name('auth.google');
Route::get('auth/google/callback', [UserController::class, 'authGoogleCallback'])->name('auth.google.callback');



Route::get('auth/github', [UserController::class, 'authGithub'])->name('auth.github');
Route::get('auth/github/callback', [UserController::class, 'authGithubCallback'])->name('auth.github.callback');
// Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');