<?php

namespace App\Http\Controllers;

use App\Jobs\SendPurchasedMail;
use App\Mail\PurchasedMail;
use App\Models\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->month){
            $date = explode("-",$request->month);
          
            $purchases = Purchase::where(['seller_id' => Auth::user()->id])->whereMonth('created_at', '=', $date[1])->whereYear('created_at', '=', $date[0])->get();
            
        }elseif($request->year){
            
            $purchases = Purchase::where(['seller_id' => Auth::user()->id])->whereYear('created_at', '=', $request->year)->get();
            
        }elseif($request->week){
            $date = explode(" - ",$request->week);
            $start = date("Y-m-d", strtotime($date[0]));  
            $end = date("Y-m-d", strtotime($date[1]));
            $purchases = Purchase::where(['seller_id' => Auth::user()->id])->whereBetween('created_at', [$start, $end])->get();
                    
        }else{
            $purchases = Purchase::where(['seller_id' => Auth::user()->id])->get();
        }
        return view('purchase.dashboard', compact('purchases'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $input = $request->except('_token');
        Purchase::create($input);
        return redirect()->route('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        return view('purchase.edit', compact('purchase'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        // dd($request);
        $purchase->update([
            'status' => $request->status,
        ]);

        if($request->status == 'purchased'){
            
            SendPurchasedMail::dispatch($purchase)->delay(now()->addSeconds(1));
        }
        session()->put('purchase.update', 'success');
        return redirect()->back();
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        $purchase->delete();
        session()->put('purchase.delete', 'success');
        return redirect()->route('purchase.index');
    }

    public function soldOut(Request $request)
    {
        if($request->month){
            $date = explode("-",$request->month);
          
            $purchases = Purchase::where(['seller_id' => Auth::user()->id,'status' => 'purchased'])->whereMonth('created_at', '=', $date[1])->whereYear('created_at', '=', $date[0])->get();
            
        }elseif($request->year){
            
            $purchases = Purchase::where(['seller_id' => Auth::user()->id,'status' => 'purchased'])->whereYear('created_at', '=', $request->year)->get();
            
        }elseif($request->week){
            $date = explode(" - ",$request->week);
            $start = date("Y-m-d", strtotime($date[0]));  
            $end = date("Y-m-d", strtotime($date[1]));
            $purchases = Purchase::where(['seller_id' => Auth::user()->id,'status' => 'purchased'])->whereBetween('created_at', [$start, $end])->get();
                    
        }else{
            $purchases = Purchase::where(['seller_id' => Auth::user()->id,'status' => 'purchased'])->get();
        }
       
        return view('purchase.sold', compact('purchases'));
    }

    public function purchased(Request $request)
    {   
        
        if($request->month){
            $date = explode("-",$request->month);
          
            $purchases = Purchase::where(['buyer_id' => Auth::user()->id,'status' => 'purchased'])->whereMonth('created_at', '=', $date[1])->whereYear('created_at', '=', $date[0])->get();
            
        }elseif($request->year){
            
            $purchases = Purchase::where(['buyer_id' => Auth::user()->id,'status' => 'purchased'])->whereYear('created_at', '=', $request->year)->get();
            
        }elseif($request->week){
            $date = explode(" - ",$request->week);
            $start = date("Y-m-d", strtotime($date[0]));  
            $end = date("Y-m-d", strtotime($date[1]));
            $purchases = Purchase::where(['buyer_id' => Auth::user()->id,'status' => 'purchased'])->whereBetween('created_at', [$start, $end])->get();
                    
        }else{
            $purchases = Purchase::where(['buyer_id' => Auth::user()->id,'status' => 'purchased'])->get();
        }
        


        return view('purchase.purchased', compact('purchases'));
    }

    public function pending()
    {
        $purchases = Purchase::where(['buyer_id' => Auth::user()->id,'status' => 'pending'])->get();
        return view('purchase.pending', compact('purchases'));
    }

    public function request()
    {
        $purchases = Purchase::where(['seller_id' => Auth::user()->id,'status' => 'pending'])->get();
        return view('purchase.request', compact('purchases'));
    }
}
