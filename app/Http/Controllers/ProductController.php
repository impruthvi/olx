<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('user:id,name', 'category:id,name', 'images:id,path,product_id')->where('seller_id', Auth::user()->id)->get();
        return view('products.dashbord', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        $seller_id = ['seller_id' => Auth::user()->id];
        $product = ($request)->merge($seller_id);
        $data = Product::create($product->all());

        if ($file = $request->file('image')) {

            $name = time() . $file->getClientOriginalName();
            $path = $request->image->storeAs('products', $name);
            Image::create([
                'path' => $path,
                'product_id' => $data->id
            ]);
        }
        $request->session()->put('product_added', 'success');
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $purchase = Purchase::where(['buyer_id' => Auth::user()->id, 'product_id' => $product->id])->first();
        return view('products.details', compact('product', 'purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('products.edit', compact('categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        // dd($product);

        $product->update($request->all());

        if ($file = $request->file('image')) {

            $name = time() . $file->getClientOriginalName();
            $path = $request->image->storeAs('products', $name);

            $image = Image::where('product_id', $product->id)->first();
            if ($image) {

                $image->update(["path" => $path]);
            } else {
                Image::create([
                    'path' => $path,
                    'product_id' => $product->id
                ]);
            }
        }

        session()->put('product.update', 'success');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        if ($product->images) {
            Storage::delete('products', $product->images->path);
            $image = Image::where('product_id', $product->id)->first();
            $image->delete();
        }
        session()->put('product.delete', 'success');
        return redirect()->route('product.index');
    }

    public function display(Request $request)
    {
        if ($request->sort == 'price') {
            $products = Product::with('user:id,name', 'category:id,name', 'images:id,path,product_id')->whereNotIn('seller_id', [Auth::user()->id])->orderBy('price', 'DESC')->get();
            $purchases = Purchase::where('buyer_id', Auth::user()->id)->get();
            return view('products.index', compact('products', 'purchases'));
        }
        elseif ($request->sort == 'a_to_z') {
            $products = Product::with('user:id,name', 'category:id,name', 'images:id,path,product_id')->whereNotIn('seller_id', [Auth::user()->id])->orderBy('name', 'ASC')->get();
            $purchases = Purchase::where('buyer_id', Auth::user()->id)->get();
            return view('products.index', compact('products', 'purchases'));
        }elseif ($request->sort == 'recent') {
            $products = Product::with('user:id,name', 'category:id,name', 'images:id,path,product_id')->whereNotIn('seller_id', [Auth::user()->id])->orderBy('created_at', 'DESC')->get();
            $purchases = Purchase::where('buyer_id', Auth::user()->id)->get();
            return view('products.index', compact('products', 'purchases'));
        } elseif ($request->sort == 'category') {
            $products = Product::with('user:id,name', 'category:id,name', 'images:id,path,product_id')->whereNotIn('seller_id', [Auth::user()->id])->get()->sortBy('category.name');
            $purchases = Purchase::where('buyer_id', Auth::user()->id)->get();
            return view('products.index', compact('products', 'purchases'));
        }else {

            $products = Product::with('user:id,name', 'category:id,name', 'images:id,path,product_id')->whereNotIn('seller_id', [Auth::user()->id])->get();
            $purchases = Purchase::where('buyer_id', Auth::user()->id)->get();
            return view('products.index', compact('products', 'purchases'));
        }
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $products = Product::with('user:id,name', 'category:id,name', 'images:id,path,product_id')->whereNotIn('seller_id', [Auth::user()->id])->where('name', 'like', '%' . $search . '%')->get();
        $purchases = Purchase::where('buyer_id', Auth::user()->id)->get();
        return view('products.search', compact('products', 'purchases'));
    }
}
