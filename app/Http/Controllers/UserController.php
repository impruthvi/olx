<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResetPassword;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class UserController extends Controller
{

    public function authGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function authGoogleCallback()
    {
        try {
    
            $user = Socialite::driver('google')->user();
            $finduser = User::where('email', $user->email)->first();
     
            if($finduser){
                
                Auth::login($finduser);
                
                return redirect()->route('products');
                
            }else{
                // dd('test');
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,                   
                    'address'=>'test',
                    'city'=>'test',
                    'state' => 'test',
                    'country' => 'test',
                    'zipcode' =>'698563',
                    'password' => encrypt('123456789')
                ]);
    
                Auth::login($newUser);
     
                return redirect()->route('products');
            }
                
    
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }
        
    }

public function authGithub()
{
    return Socialite::driver('github')->redirect();
    
}

public function authGithubCallback()
{
    try {
    
        $user = Socialite::driver('github')->user();
        $finduser = User::where('email', $user->email)->first();
 
        if($finduser){
            
            Auth::login($finduser);
            
            return redirect()->route('products');
            
        }else{
            // dd('test');
            $newUser = User::create([
                'name' => $user->name,
                'email' => $user->email,                   
                'address'=>'test',
                'city'=>'test',
                'state' => 'test',
                'country' => 'test',
                'zipcode' =>'698563',
                'password' => encrypt('123456789')
            ]);

            Auth::login($newUser);
 
            return redirect()->route('products');
        }
            


    } catch (Exception $e) {
        dd($e->getMessage());
    }
    
    
}

    public function profile()
    {
        return view('user.profile');
    }

    public function editProfile()
    {
        return view('user.profile_edit');
    }

    public function passwordReset(Request $request)
    {
        return view('user.password_reset');
    }

    public function update(UserRequest $request, User $user)
    {
        
        $input = $request->all();
        $user = User::findOrFail(Auth::user()->id);
        if ($file = $request->file('profile')) {
            $name = time() . $file->getClientOriginalName();
            $path = $request->profile->storeAs('profiles', $name);
            $input['profile'] = $path;
        }

        $user->update($input);
        session()->put('profile.update', 'success');
        return redirect()->back();
    }

    public function storeResetPassword(ResetPassword $request)
    {
        $user = User::findOrFail(Auth::user()->id);
       if(Hash::check($request->old_password,$user->password)) {
            $password = bcrypt($request->password);
            $user->update([
                'password' => $password
            ]);

            session()->put('password.update', 'success');
            return redirect()->route('profile');

        }else{
             session()->put('password.update', 'fail');
            return redirect()->back();

        }
    }

    public function change(Request $request)
    {
        // dd($request->all());
        session()->put('user.change', $request->submit);
        return redirect()->back();
    }



 
}
