<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/^[a-zA-Z\s]*$/|max:255|min:3',
            'email' => 'required|string|email|max:255|min:3|unique:users',
            'number' => 'required|digits:10',
            'address' => 'required|max:255|min:3',
            'city' => 'required|regex:/^[a-zA-Z\s]*$/|max:255|min:3',
            'state' => 'required|regex:/^[a-zA-Z\s]*$/|max:255|min:3',
            'country' => 'required|regex:/^[a-zA-Z\s]*$/|max:255|min:3',
        ];
    }
}
