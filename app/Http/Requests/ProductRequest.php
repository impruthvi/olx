<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|min:3|max:100|regex:/^[a-zA-Z\s]*$/',
                    'price' => 'required|numeric',
                    'cat_id' => 'required|integer',
                    'image' => 'required|mimes:jpeg,jpg,png',
                    'description' => 'required|min:3|max:500',
                ];
                break;
            case 'PUT':
                return [
                    'name' => 'required|min:3|max:100|regex:/^[a-zA-Z\s]*$/',
                    'price' => 'required|numeric',
                    'cat_id' => 'required|integer',
                    'description' => 'required|min:3|max:500',
                ];
                break;

        }

    }
}
