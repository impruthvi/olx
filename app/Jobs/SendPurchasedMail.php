<?php

namespace App\Jobs;

use App\Mail\PurchasedMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendPurchasedMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $purchase;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($purchase)
    {
        $this->purchase = $purchase; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->purchase->buyer->email)->send(new PurchasedMail($this->purchase));
    }
}
