<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'name', 'description', 'price', 'seller_id', 'cat_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'seller_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'cat_id', 'id');
    }

    public function images()
    {
        return $this->belongsTo(Image::class,'id','product_id');
    }

}
